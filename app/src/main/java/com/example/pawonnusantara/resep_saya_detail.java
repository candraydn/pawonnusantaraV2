package com.example.pawonnusantara;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class resep_saya_detail extends AppCompatActivity {
    private DBResep_saya MyDatabase;
    private ArrayList<DataFilter> dataList;
    TextView mjudul, mdesc, mbahan, mstep;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resep_saya_detail);
        dataList = new ArrayList<>();
        MyDatabase = new DBResep_saya(getBaseContext());
        mjudul = findViewById(R.id.judul);
        mdesc = findViewById(R.id.deskripsi);
        mbahan = findViewById(R.id.bahan);
        mstep = findViewById(R.id.step);
        ImageView foto = findViewById(R.id.imageUtama);

        String GetNama = getIntent().getExtras().getString("nama");
        SQLiteDatabase ReadData = MyDatabase.getReadableDatabase();
        Cursor cursor = ReadData.rawQuery("SELECT * FROM "+ DBResep_saya.MyColumns.NamaTabel+" where "+DBResep_saya.MyColumns.NamaResep+"='"+GetNama+"'",null);
        cursor.moveToFirst();//Memulai Cursor pada Posisi Awal
        dataList.add(new DataFilter(cursor.getString(0),
                cursor.getString(1), cursor.getString(2),cursor.getString(3),
                cursor.getString(4), cursor.getBlob(5)));

        final String Nama = dataList.get(0).getNama();
        final String Asal = dataList.get(0).getAsal();
        final String Desc = dataList.get(0).getDesc();
        final String Bahan = dataList.get(0).getBahan();
        final String Langkah = dataList.get(0).getLangkah();
        final byte[] Foto = dataList.get(0).getFoto();

        mjudul.setText(Nama+" ("+Asal+")");
        mdesc.setText(Desc);
        mbahan.setText(Bahan);
        mstep.setText(Langkah);
        Bitmap bx = BitmapFactory.decodeByteArray(Foto, 0, Foto.length);
        foto.setImageBitmap(bx);
    }

    @Override
    public void onBackPressed() {
        Intent inte = new Intent(resep_saya_detail.this, resep_saya.class);
        startActivity(inte);
        finish();
    }
}
