package com.example.pawonnusantara;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Intent;

import java.util.ArrayList;

public class resepDetail extends AppCompatActivity {

    private dbHelper MyDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resep_detail);
        MyDatabase = new dbHelper(getBaseContext());
        getData();
    }
    protected void getData(){
        Bundle bundle=getIntent().getExtras();
        String inte = bundle.getString("id");

        //Mengambil Repository dengan Mode Membaca
        SQLiteDatabase ReadData = MyDatabase.getReadableDatabase();
        Cursor cursor = ReadData.rawQuery("SELECT * FROM resep where id ="+inte,null);

        cursor.moveToFirst();//Memulai Cursor pada Posisi Awal

        String judulResep = cursor.getString(1);
        String asalResep = cursor.getString(9);
        TextView judul = findViewById(R.id.judul);
        judul.setText(judulResep+" ("+asalResep+")");

        String descResep = cursor.getString(2);
        TextView desc = findViewById(R.id.deskripsi);
        desc.setText(descResep);

        String bahanResep = cursor.getString(7);
        String[] separatedBahan = bahanResep.split(";");
        int sizeBahan = separatedBahan.length;
        TextView bahan = findViewById(R.id.bahan);

        for(int i = 0; i < sizeBahan; i++) {
            bahan.append("\n"+separatedBahan[i]);
        }

        String stepResep = cursor.getString(8);
        String[] separatedStep = stepResep.split(";");
        int sizeStep = separatedStep.length;
        TextView step = findViewById(R.id.step);

        for(int i = 0; i < sizeStep; i++) {
            step.append("\n"+separatedStep[i]);
        }

        String srcResep = cursor.getString(4);
        TextView src = findViewById(R.id.sumber);
        src.setText(srcResep);

        ImageView foto = findViewById(R.id.imageUtama);
        final byte[] imagi = (byte[]) cursor.getBlob(3);
        Bitmap by = BitmapFactory.decodeByteArray(imagi, 0, imagi.length);
        foto.setImageBitmap(by);

//        //Melooping Sesuai Dengan Jumlan Data (Count) pada cursor
//        for(int count=0; count < cursor.getCount(); count++){
//
//            cursor.moveToPosition(count);//Berpindah Posisi dari no index 0 hingga no index terakhir
//
//
//            NamaList.add(cursor.getString(1));//Menambil Data Dari Kolom 0 (NIM)
//            AsalList.add(cursor.getString(9));//Menambil Data Dari Kolom 1 (Nama)
//            TingkatanList.add(cursor.getBlob(10));//Menambil Data Dari Kolom 2 (Jurusan)
//            ImageList.add(cursor.getBlob(3));
//            IdList.add(cursor.getString(0));
//        }
        cursor.close();
        MyDatabase.close();
    }
}
