package com.example.pawonnusantara;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class update_resep_saya extends AppCompatActivity {
    private DBResep_saya MyDatabase;
    ImageView mImgChoosed;
    Button mBtnChoose, mBtnInput;
    EditText mNama, mAsal, mDesc, mBahan, mLangkah;
    private ArrayList<DataFilter> dataList;

    private String cNama, cDesc, cAsal, cBahan, cLangkah;

    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_resep_saya);
        dataList = new ArrayList<>();
        MyDatabase = new DBResep_saya(getBaseContext());
        mImgChoosed = findViewById(R.id.chooseImg);
        mBtnChoose = findViewById(R.id.btnUpload);
        mBtnInput = findViewById(R.id.btnInput);
        mNama = findViewById(R.id.namaResep);
        mAsal = findViewById(R.id.asalResep);
        mDesc = findViewById(R.id.descResep);
        mBahan = findViewById(R.id.bahanResep);
        mLangkah = findViewById(R.id.langkahResep);

        String GetNama = getIntent().getExtras().getString("nama");
        SQLiteDatabase ReadData = MyDatabase.getReadableDatabase();
        Cursor cursor = ReadData.rawQuery("SELECT * FROM "+ DBResep_saya.MyColumns.NamaTabel+" where "+DBResep_saya.MyColumns.NamaResep+"='"+GetNama+"'",null);
        cursor.moveToFirst();//Memulai Cursor pada Posisi Awal
        dataList.add(new DataFilter(cursor.getString(0),
                cursor.getString(1), cursor.getString(2),cursor.getString(3),
                cursor.getString(4), cursor.getBlob(5)));
        final String Nama = dataList.get(0).getNama();
        final String Asal = dataList.get(0).getAsal();
        final String Desc = dataList.get(0).getDesc();
        final String Bahan = dataList.get(0).getBahan();
        final String Langkah = dataList.get(0).getLangkah();
        final byte[] Foto = dataList.get(0).getFoto();
        Bitmap by = BitmapFactory.decodeByteArray(Foto, 0, Foto.length);

        mNama.setText(Nama);
        mAsal.setText(Asal);
        mDesc.setText(Desc);
        mBahan.setText(Bahan);
        mLangkah.setText(Langkah);
        mImgChoosed.setImageBitmap(by);

        mBtnInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpdateData();
                startActivity(new Intent(update_resep_saya.this, resep_saya.class));
                finish();
            }
        });
        mBtnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check permission
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_DENIED) {
                        //permission not granted
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permissions, PERMISSION_CODE);
                    } else {
                        //permission already granted
                        pickImagefromGalery();
                    }
                } else {

                }
            }
        });
    }
    private void setUpdateData(){
        cNama = mNama.getText().toString().trim();
        cAsal = mAsal.getText().toString().trim();
        cDesc = mDesc.getText().toString().trim();
        cBahan = mBahan.getText().toString().trim();
        cLangkah = mLangkah.getText().toString().trim();

        //Menerima Data NIM yang telah dipilih Oleh User untuk diposes
        String GetNIM = getIntent().getExtras().getString("nama");

        SQLiteDatabase database = MyDatabase.getReadableDatabase();

        //Memasukan Data baru pada 3 kolom (NIM, Nama dan Jurusan)
        ContentValues values = new ContentValues();
        values.put(DBResep_saya.MyColumns.NamaResep, cNama);
        values.put(DBResep_saya.MyColumns.AsalResep, cAsal);
        values.put(DBResep_saya.MyColumns.DescResep, cDesc);
        values.put(DBResep_saya.MyColumns.BahanResep, cBahan);
        values.put(DBResep_saya.MyColumns.LangkahResep, cLangkah);

        //Untuk Menentukan Data/Item yang ingin diubah, berdasarkan NIM
        String selection = DBResep_saya.MyColumns.NamaResep + " LIKE ?";
        String[] selectionArgs = {GetNIM};
        database.update(DBResep_saya.MyColumns.NamaTabel, values, selection, selectionArgs);
        Toast.makeText(getApplicationContext(), "Berhasil Diubah", Toast.LENGTH_SHORT).show();
    }
    private void pickImagefromGalery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] ==
                        getPackageManager().PERMISSION_GRANTED) {
                    pickImagefromGalery();
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE){
            mImgChoosed.setImageURI(data.getData());
        }
    }

    @Override
    public void onBackPressed() {
        Intent inte = new Intent(update_resep_saya.this, resep_saya.class);
        startActivity(inte);
        finish();
    }
}
