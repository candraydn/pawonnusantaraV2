package com.example.pawonnusantara;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class recyclerViewAdapter extends RecyclerView.Adapter<recyclerViewAdapter.ViewHolder>{

    private ArrayList namaList; //Digunakan untuk Nama
    private ArrayList asalList; //Digunakan untuk Asal
    private ArrayList tingkatanList; //Digunakan untuk Tingakatan
    private ArrayList imgList;
    private ArrayList idList;
    private Context context;

    //Membuat Konstruktor pada Class RecyclerViewAdapter
    recyclerViewAdapter(ArrayList namaList, ArrayList asalList, ArrayList tingkatanList, ArrayList imgList, ArrayList idList){
        this.namaList = namaList;
        this.asalList = asalList;
        this.tingkatanList = tingkatanList;
        this.imgList = imgList;
        this.idList = idList;
    }

    //ViewHolder Digunakan Untuk Menyimpan Referensi Dari View-View
    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView Nama, Asal;
        private ImageView Image, Tingkatan;
        private RelativeLayout Rela;

        ViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            //Menginisialisasi View-View untuk kita gunakan pada RecyclerView
            Nama = itemView.findViewById(R.id.namaResep);
            Asal = itemView.findViewById(R.id.asalResep);
            Tingkatan = itemView.findViewById(R.id.image01);
            Image = itemView.findViewById(R.id.image00);
            Rela = itemView.findViewById(R.id.item_list);
        }
    }
    @NonNull
    @Override
    public recyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //Membuat View untuk Menyiapkan dan Memasang Layout yang Akan digunakan pada RecyclerView
        View V = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_content, viewGroup, false);
        return new ViewHolder(V);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        //Memanggil Nilai/Value Pada View-View Yang Telah Dibuat pada Posisi Tertentu
        final String Nama = (String) namaList.get(position);
        final String Asal = (String) asalList.get(position);
        final byte[] imagi = (byte[]) tingkatanList.get(position);
        final byte[] imag = (byte[]) imgList.get(position);
        final String Id = (String) idList.get(position);
        Bitmap by = BitmapFactory.decodeByteArray(imagi, 0, imagi.length);
        Bitmap bt = BitmapFactory.decodeByteArray(imag, 0, imag.length);
        holder.Nama.setText(Nama);
        holder.Asal.setText("Khas "+Asal);
        holder.Tingkatan.setImageBitmap(by);
        holder.Image.setImageBitmap(bt);

        //setonclick DETAIL
        holder.Rela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Intent dataForm = new Intent(v.getContext(), resepDetail.class);
                dataForm.putExtra("id", Id);
                context.startActivity(dataForm);
                ((Activity)context).finish();
            }
        }
        );
    }

    @Override
    public int getItemCount() {

        //Menghitung Ukuran/Jumlah Data Yang Akan Ditampilkan Pada RecyclerView
        return namaList.size();
    }

    public void clearData(){
        namaList.clear();
        asalList.clear();
        tingkatanList.clear();
        imgList.clear();
    }

}
