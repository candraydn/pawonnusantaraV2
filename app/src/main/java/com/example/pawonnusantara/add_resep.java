package com.example.pawonnusantara;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class add_resep extends AppCompatActivity {

    ImageView mImgChoosed;
    Button mBtnChoose, mBtnInput;
    EditText mNama, mAsal, mDesc, mBahan, mLangkah, mFoto;
    byte[] imgNih;

    private String cNama, cDesc, cAsal, cBahan, cLangkah, cFoto;

    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;

    private DBResep_saya dbresep;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_resep);

        mImgChoosed = findViewById(R.id.chooseImg);
        mBtnChoose = findViewById(R.id.btnUpload);
        mBtnInput = findViewById(R.id.btnInput);
        mNama = findViewById(R.id.namaResep);
        mAsal = findViewById(R.id.asalResep);
        mDesc = findViewById(R.id.descResep);
        mBahan = findViewById(R.id.bahanResep);
        mLangkah = findViewById(R.id.langkahResep);

        dbresep = new DBResep_saya(getBaseContext());



        mBtnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check permission
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_DENIED) {
                        //permission not granted
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permissions, PERMISSION_CODE);
                    } else {
                        //permission already granted
                        pickImagefromGalery();
                    }
                } else {

                }
            }
        });
        mBtnInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    setData();
                    saveData();
                    Toast.makeText(getApplicationContext(),"Data Berhasil Disimpan", Toast.LENGTH_SHORT).show();
//                    mNama.setText("");
//                    mAsal.setText("");
//                    mDesc.setText("");
//                    mBahan.setText("");
//                    mLangkah.setText("");
//                    mImgChoosed.setImageResource(R.drawable.ic_inser_photo);
                    Intent inte = new Intent(add_resep.this, resep_saya.class);
                    startActivity(inte);
                    finish();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),"Ada kesalahan, cek kemabali form Anda (Foto max 1 mb)", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void setData(){
        imgNih = imageViewToByte(mImgChoosed);
        cNama = mNama.getText().toString().trim();
        cAsal = mAsal.getText().toString().trim();
        cDesc = mDesc.getText().toString().trim();
        cBahan = mBahan.getText().toString().trim();
        cLangkah = mLangkah.getText().toString().trim();
    }

    @Override
    public void onBackPressed() {
        Intent inte = new Intent(add_resep.this, resep_saya.class);
        startActivity(inte);
        finish();
    }

    public static byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    //Berisi Statement-Statement Untuk Menyimpan Data Pada Database
    private void saveData(){
        //Mendapatkan Repository dengan Mode Menulis
        SQLiteDatabase create = dbresep.getWritableDatabase();

        //Membuat Map Baru, Yang Berisi Nama Kolom dan Data Yang Ingin Dimasukan
        ContentValues values = new ContentValues();
        values.put(DBResep_saya.MyColumns.NamaResep, cNama);
        values.put(DBResep_saya.MyColumns.AsalResep, cAsal);
        values.put(DBResep_saya.MyColumns.DescResep, cDesc);
        values.put(DBResep_saya.MyColumns.BahanResep, cBahan);
        values.put(DBResep_saya.MyColumns.LangkahResep, cLangkah);
        values.put(DBResep_saya.MyColumns.FotoResep, imgNih);

        //Menambahkan Baris Baru, Berupa Data Yang Sudah Diinputkan pada Kolom didalam Database
        create.insert(DBResep_saya.MyColumns.NamaTabel, null, values);
    }

    private void pickImagefromGalery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] ==
                        getPackageManager().PERMISSION_GRANTED) {
                    pickImagefromGalery();
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE){
            mImgChoosed.setImageURI(data.getData());
        }
    }
}
