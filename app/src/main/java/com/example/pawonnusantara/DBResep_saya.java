package com.example.pawonnusantara;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class DBResep_saya extends SQLiteOpenHelper{
    static abstract class MyColumns implements BaseColumns {
            //Menentukan Nama Table dan Kolom
            static final String NamaTabel = "resep_saya";
            static final String NamaResep = "nama_resep";
        static final String AsalResep = "asal_resep";
        static final String DescResep = "desc_resep";
        static final String BahanResep = "bahan_resep";
        static final String LangkahResep = "langkah_resep";
        static final String FotoResep = "foto_resep";
    }
    private static final String NamaDatabse = "resep_saya.db";
    private static final int VersiDatabase = 14;

    //Query yang digunakan untuk membuat Tabel
    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE "+MyColumns.NamaTabel+
            "("+MyColumns.NamaResep+" TEXT PRIMARY KEY, "+MyColumns.AsalResep+" TEXT NOT NULL, "+MyColumns.DescResep+
            " TEXT NOT NULL, "+MyColumns.BahanResep+" TEXT NOT NULL, "+MyColumns.LangkahResep+
            " TEXT NOT NULL, "+MyColumns.FotoResep+" BLOB NOT NULL)";

    //Query yang digunakan untuk mengupgrade Tabel
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "+MyColumns.NamaTabel;

    DBResep_saya(Context context) {
        super(context, NamaDatabse, null, VersiDatabase);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}
