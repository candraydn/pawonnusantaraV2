package com.example.pawonnusantara;

import java.sql.Blob;

public class DataFilter {
    private String NamaResep;
    private String AsalResep;
    private String DescResep;
    private String BahanResep;
    private String LangkahResep;
    private  byte[] FotoResep;

    DataFilter(String Nama, String Asal, String Desc, String Bahan, String Langkah, byte[] Foto) {
        this.NamaResep = Nama;
        this.AsalResep = Asal;
        this.DescResep = Desc;
        this.BahanResep = Bahan;
        this.LangkahResep = Langkah;
        this.FotoResep = Foto;
    }

    String getNama() {
        return NamaResep;
    }

    public String getAsal() {
        return AsalResep;
    }

    public String getDesc() {
        return DescResep;
    }

    public String getBahan() {
        return BahanResep;
    }

    public String getLangkah() {
        return LangkahResep;
    }

    public byte[] getFoto(){ return FotoResep; }
}
