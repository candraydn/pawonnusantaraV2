package com.example.pawonnusantara;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends Activity {

    private dbHelper MyDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn1 = findViewById(R.id.btnMakanan);
        Button btn2 = findViewById(R.id.btnMinuman);
        Button btn3 = findViewById(R.id.btnResepku);
        TextView txtMakanan = findViewById(R.id.countMakanan);
        TextView txtMinuman = findViewById(R.id.countMinuman);
        TextView txtResepku = findViewById(R.id.countResepku);
        MyDatabase = new dbHelper(getBaseContext());
        txtMakanan.setText(Integer.toString(countMakanan())+" Resep");
        txtMinuman.setText(Integer.toString(countMinuman())+" Resep");
        txtResepku.setText(Integer.toString(countReseku())+" Resep");

        btn1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0){
                Intent inte = new Intent(MainActivity.this, dataMakanan.class);
                inte.putExtra("nama", "0");
                startActivity(inte);
            }
        });
        btn2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0){
                Intent inte = new Intent(MainActivity.this, dataMakanan.class);
                inte.putExtra("nama", "1");
                startActivity(inte);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0){
                Intent inte = new Intent(MainActivity.this, resep_saya.class);
                startActivity(inte);
            }
        });
    }
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finish();
                System.exit(1);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }
    public int countMakanan(){
        SQLiteDatabase ReadData = MyDatabase.getReadableDatabase();
        Cursor cursor = ReadData.rawQuery("SELECT COUNT(*) FROM resep where status_resep = 0",null) ;
        cursor.moveToFirst();
        // return count
        return cursor.getInt(0);
    }
    public int countMinuman(){
        SQLiteDatabase ReadData = MyDatabase.getReadableDatabase();
        Cursor cursor = ReadData.rawQuery("SELECT COUNT(*) FROM resep where status_resep = 1",null) ;
        cursor.moveToFirst();
        // return count
        return cursor.getInt(0);
    }
    public int countReseku(){
        SQLiteDatabase ReadData = MyDatabase.getReadableDatabase();
        Cursor cursor = ReadData.rawQuery("SELECT COUNT(*) FROM resep where status_resep = 2",null) ;
        cursor.moveToFirst();
        // return count
        return cursor.getInt(0);
    }
}
