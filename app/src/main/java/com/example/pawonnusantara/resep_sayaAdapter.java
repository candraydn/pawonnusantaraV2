package com.example.pawonnusantara;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.sql.Blob;
import java.util.ArrayList;

public class resep_sayaAdapter extends RecyclerView.Adapter<resep_sayaAdapter.ViewHolder> {
    private ArrayList<DataFilter> dataList;
    private Context context;

    //Membuat Konstruktor pada Class RecyclerViewAdapter
    resep_sayaAdapter(ArrayList<DataFilter> dataList){
        this.dataList = dataList;
    }

    //ViewHolder Digunakan Untuk Menyimpan Referensi Dari View-View
    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView Nama, Asal, Substring;
        private ImageView Image, Tingkatan;
        private RelativeLayout Rela;
        private ImageButton Overflow;

        ViewHolder(View itemView) {
            super(itemView);

            //Mendapatkan Context dari itemView yang terhubung dengan Activity ViewData
            context = itemView.getContext();

            //Menginisialisasi View-View untuk kita gunakan pada RecyclerView
            Nama = itemView.findViewById(R.id.namaResep);
            Asal = itemView.findViewById(R.id.asalResep);
            Tingkatan = itemView.findViewById(R.id.image01);
            Image = itemView.findViewById(R.id.image00);
            Rela = itemView.findViewById(R.id.item_list);
            Overflow = itemView.findViewById(R.id.overflow);
            Substring = itemView.findViewById(R.id.subStr);
        }
    }

    @NonNull
    @Override
    public resep_sayaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //Membuat View untuk Menyiapkan dan Memasang Layout yang Akan digunakan pada RecyclerView
        View V = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_resep_saya, viewGroup, false);
        return new resep_sayaAdapter.ViewHolder(V);
    }

    @Override
    public void onBindViewHolder(resep_sayaAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        //Memanggil Nilai/Value Pada View-View Yang Telah Dibuat pada Posisi Tertentu
        final String Nama = dataList.get(position).getNama();
        final String Asal = dataList.get(position).getAsal();
        final String Substrings = dataList.get(position).getDesc();
        final byte[] imagi = dataList.get(position).getFoto();
//        final byte[] imag = (byte[]) imgList.get(position);
//        final String Id = (String) idList.get(position);
        Bitmap by = BitmapFactory.decodeByteArray(imagi, 0, imagi.length);
//        Bitmap bt = BitmapFactory.decodeByteArray(imag, 0, imag.length);
        holder.Nama.setText(Nama);
        holder.Asal.setText("Khas " + Asal);
//        holder.Tingkatan.setImageBitmap(by);
        holder.Image.setImageBitmap(by);
        final String substrfix = Substrings.substring(0,10);
        holder.Substring.setText(substrfix+" ...");

        //setonclick DETAIL
        holder.Rela.setOnClickListener(
                new View.OnClickListener() {
                                                             @Override
                                                             public void onClick(final View v) {
                                                                 Intent dataForm = new Intent(v.getContext(), resep_saya_detail.class);
                                                                 dataForm.putExtra("nama", Nama);
                                                                 context.startActivity(dataForm);
                                                                 ((Activity) context).finish();
                                                             }
                                                         }
        );
        //Mengimplementasikan Menu Popup pada Overflow (ImageButton)
        holder.Overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                //Membuat Instance/Objek dari PopupMenu
                PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
                popupMenu.inflate(R.menu.menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.delete:
                                //Menghapus Data Dari Database
                                DBResep_saya getDatabase = new DBResep_saya(view.getContext());
                                SQLiteDatabase DeleteData = getDatabase.getWritableDatabase();
                                //Menentukan di mana bagian kueri yang akan dipilih
                                String selection = DBResep_saya.MyColumns.NamaResep + " LIKE ?";
                                //Menentukan Nama Dari Data Yang Ingin Dihapus
                                String[] selectionArgs = {Nama};
                                DeleteData.delete(DBResep_saya.MyColumns.NamaTabel, selection, selectionArgs);

                                //Menghapus Data pada List dari Posisi Tertentu
                                String position2 = String.valueOf(Nama.indexOf(Nama));
                                dataList.remove(position);
                                notifyItemRemoved(position);
                                if (position2 == null) {
                                    notifyItemRangeChanged(Integer.parseInt(position2), dataList.size());
                                }
                                break;

                            case R.id.update:
                                Intent dataForm = new Intent(view.getContext(), update_resep_saya.class);
                                dataForm.putExtra("nama", Nama);
                                context.startActivity(dataForm);
                                ((Activity)context).finish();
                                break;
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
    }
    public int getItemCount() {
        //Menghitung Ukuran/Jumlah Data Yang Akan Ditampilkan Pada RecyclerView
        return dataList.size();
    }

    void setFilter(ArrayList<DataFilter> filterList){
        dataList = new ArrayList<>();
        dataList.addAll(filterList);
        notifyDataSetChanged();
    }
}
